module.exports = {
    maskRemove: (data, ignored) => {

        if(Array.isArray(data)) {
            return
        }
        else if(typeof data === 'object') {
            Object.keys(data).forEach(key => {
                if(ignored && ignored.includes(key))
                    return

                if(!Array.isArray(data[key]))
                    data[key] = data[key].replace(/[\.\/\-\(\)]/g, '')
            })
        }
        else if(typeof data === 'string') {
            return data.replace(/[\.\/\-\(\)]/g, '')
        }
        else {
            throw new Error('Parâmetro inválido para a função maskRemove().')
        }
    },

    makeToast: (msg, type) => {
        let color

        switch(type) {
            case 'success': color = 'green'
                            break
            case 'error': color = 'red'
                          break
            case 'warning': color = 'yellow black-text'
                          break
            case 'info': color = 'blue'
                          break
            default: color = 'blue'
                     break
        }

        return `${color}|${msg}`
    },

    toastParseType: type => {
        let color

        switch(type) {
            case 'success': color = 'green'
                            break
            case 'error': color = 'red'
                          break
            case 'warning': color = 'yellow black-text'
                          break
            case 'info': color = 'blue'
                          break
            default: color = 'yellow black-text'
                     break
        }

        return color
    },

    translateMessage: (msg) => {
        switch(msg.toLowerCase()) {
            case 'file too large': 
                msg = 'O arquivo deve ter até 2Mb.'
                break
            case 'unexpected field': 
                msg = 'Você pode enviar no máximo 5 arquivos.'
                break
            default: 
                break
        }

        return msg
    },

    removeWhiteSpaces: (str) => {
        str.replace(/ /g, '')
    },

    parseEntityToText: (entity) => {
        switch(entity) {
            case 'Associacao': return 'Associação'
            case 'SegmentoMercado': return 'Segmento de Mercado'
            case 'Funcao': return 'Função'
            case 'CategoriaProduto': return 'Categoria'
        }
    }

}