function getFirstPhone(phonesString) {
    let phoneRegex = /\(\d{2}\)( )?\d{4,5}-\d{4}/g
    let result = phoneRegex.exec(phonesString)

    if(result && result.length) {
        return result[0]
    }
}

function scrollToSection(sectionNumber) {
    let position

    switch(sectionNumber) {
        case 1: 
            position = 660
            break
        case 2: 
            position = 852
            break
        case 3: 
            position = 1653
            break
        default:
            position = 0
            break
    }

    $('html, body').animate({ 
        scrollTop: position
    }, 1000)
}
