const Sequelize = require('sequelize')
const mysqlConfig = require('../config/mysql.js')
const sequelize = new Sequelize(mysqlConfig)

class Associacao extends Sequelize.Model {}
class SegmentoMercado extends Sequelize.Model {}
class Cliente extends Sequelize.Model {}
class ClientePf extends Sequelize.Model {}
class ClientePj extends Sequelize.Model {}
class ClienteEndereco extends Sequelize.Model {}
class ClienteImagem extends Sequelize.Model {}
class ClienteContato extends Sequelize.Model {}
class Funcao extends Sequelize.Model {}
class RelacionamentoCliente extends Sequelize.Model {}
class Conta extends Sequelize.Model {}

class Orcamento extends Sequelize.Model {}
class ItemOrcamento extends Sequelize.Model {}
class ParametroOrcamento extends Sequelize.Model {}

class Produto extends Sequelize.Model {}
class CategoriaProduto extends Sequelize.Model {}
class ProdutoImagem extends Sequelize.Model {}
class Fabricante extends Sequelize.Model {}
class Contratacao extends Sequelize.Model {}
class Pacote extends Sequelize.Model {}
class Promocao extends Sequelize.Model {}

class RobotContrato extends Sequelize.Model {}
class RobotProduto extends Sequelize.Model {}


Associacao.init({ 
    descricao: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

CategoriaProduto.init({ 
    descricao: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

SegmentoMercado.init({ 
    descricao: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Cliente.init({ 
    pessoa: {
        type: Sequelize.STRING
    },
    facebook: {
        type: Sequelize.STRING
    },
    site: {
        type: Sequelize.STRING
    }
    // references: {
    //     // This is a reference to another model
    //     model: Bar,
   
    //     // This is the column name of the referenced model
    //     key: 'id',
   
    //     // This declares when to check the foreign key constraint. PostgreSQL only.
    //     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    //   }
}, { sequelize, timestamps: false, freezeTableName: true })

ClientePf.init({ 
    nome: {
        type: Sequelize.STRING
    },
    cpf: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

ClientePj.init({ 
    razaoSocial: {
        type: Sequelize.STRING
    },
    nomeFantasia: {
        type: Sequelize.STRING
    },
    cnpj: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

ClienteContato.init({ 
    tipo: {
        type: Sequelize.STRING,
        comment: 'E-mail, celular, fixo, fax, WhatsApp, etc.'
    },
    valor: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

ClienteEndereco.init({ 
    cep: {
        type: Sequelize.STRING
    },
    uf: {
        type: Sequelize.STRING
    },
    bairro: {
        type: Sequelize.STRING
    },
    logradouro: {
        type: Sequelize.STRING
    },
    numero: {
        type: Sequelize.STRING
    },
    complemento: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

ClienteImagem.init({ 
    urlImagem: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Funcao.init({ 
    descricao: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

RelacionamentoCliente.init({ 
    
}, { sequelize, timestamps: false, freezeTableName: true })
RelacionamentoCliente.removeAttribute('id')

Conta.init({ 
    email: {
        type: Sequelize.STRING
    },
    senha: {
        type: Sequelize.STRING
    },
    dataValidadeSenha: {
        type: Sequelize.DATE
    },
    dataCriacao: {
        type: Sequelize.DATE
    },
    ativo: {
        type: Sequelize.BOOLEAN
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Orcamento.init({ 
    valor_total: {
        type: Sequelize.DECIMAL
    },
    data_hora: {
        type: Sequelize.DATE
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Fabricante.init({ 
    nome: {
        type: Sequelize.STRING
    },
    sac: {
        type: Sequelize.STRING
    },
    site: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Produto.init({ 
    nome: {
        type: Sequelize.STRING
    },
    decricao: {
        type: Sequelize.STRING
    },
    decricao_tecnica: {
        type: Sequelize.STRING
    },
    titulo_anuncio: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })

Pacote.init({ 
    minimo: {
        type: Sequelize.INTEGER
    },
    maximo: {
        type: Sequelize.INTEGER
    },
    preco: {
        type: Sequelize.DECIMAL
    }
}, { sequelize, timestamps: false, freezeTableName: true })

ItemOrcamento.init({ 
    valor_item: {
        type: Sequelize.DECIMAL
    }
}, { sequelize, timestamps: false, freezeTableName: true })
ItemOrcamento.removeAttribute('id')

ParametroOrcamento.init({ 
    descricao: {
        type: Sequelize.STRING
    },
    valor: {
        type: Sequelize.STRING
    }
}, { sequelize, timestamps: false, freezeTableName: true })
ParametroOrcamento.removeAttribute('id')

ProdutoImagem.init({ 
    urlImagem: {
        type: Sequelize.STRING
    },
}, { sequelize, timestamps: false, freezeTableName: true })

Contratacao.init({ 
    inicio: {
        type: Sequelize.DATE
    },
    fim: {
        type: Sequelize.DATE
    }
}, { sequelize, timestamps: false, freezeTableName: true })
Contratacao.removeAttribute('id')

Promocao.init({ 
    inicio: {
        type: Sequelize.DATE
    },
    fim: {
        type: Sequelize.DATE
    },
    percentual_desconto: {
        type: Sequelize.DECIMAL
    }
}, { sequelize, timestamps: false, freezeTableName: true })
Promocao.removeAttribute('id')

RobotProduto.init({ 
    descricao: {
        type: Sequelize.DATE
    },
    ativo: {
        type: Sequelize.BOOLEAN
    }
}, { sequelize, timestamps: false, freezeTableName: true })

RobotContrato.init({ 
    string_con: {
        type: Sequelize.STRING
    },
    inicio: {
        type: Sequelize.DATE
    },
    fim: {
        type: Sequelize.DATE
    }
}, { sequelize, timestamps: false, freezeTableName: true })


//Restrições de integridade (FKs) ===============================

Associacao.hasMany(Cliente, { foreignKey: 'associacao_id' })
Cliente.belongsTo(Associacao, { foreignKey: 'associacao_id' })
SegmentoMercado.hasMany(Cliente, { foreignKey: 'segmento_id' })
Cliente.belongsTo(SegmentoMercado, { foreignKey: 'segmento_id' })
Cliente.hasMany(ClienteContato, { foreignKey: 'cliente_id' })
Cliente.hasOne(ClienteEndereco, { foreignKey: 'cliente_id' })
Cliente.hasMany(ClienteImagem, { foreignKey: 'cliente_id' })
ClienteImagem.belongsTo(Cliente, { foreignKey: 'cliente_id' })
Cliente.hasMany(ClientePf, { foreignKey: 'cliente_id' })
Cliente.hasOne(ClientePj, { foreignKey: 'cliente_id' })
Cliente.hasOne(Conta, { foreignKey: 'cliente_id' })
Cliente.hasMany(RelacionamentoCliente, { foreignKey: 'fornecedor_id' })
Cliente.hasMany(RelacionamentoCliente, { foreignKey: 'cliente_id' })
Funcao.hasMany(ClienteContato, { foreignKey: 'funcao_id' })
Cliente.hasMany(Orcamento,{foreignKey: 'cliente_id'})
CategoriaProduto.hasMany(Produto, {foreignKey: 'categoriaproduto_id'})
Fabricante.hasMany(Produto, {foreignKey: 'fabricante_id'})
Produto.hasMany(Pacote, {foreignKey : 'produto_id'})
Orcamento.hasMany(ItemOrcamento, {foreignKey : 'orcamento_id'})
Pacote.hasMany(ItemOrcamento, { foreignKey : 'pacote_id'})
Cliente.hasMany(ParametroOrcamento, {foreignKey : 'cliente_id'})
Produto.hasMany(ParametroOrcamento, {foreignKey : 'produto_id'})
Produto.hasMany(ProdutoImagem, { foreignKey: 'produto_id'})
Cliente.hasMany(Contratacao, {foreignKey : 'cliente_id'})
Cliente.hasMany(Contratacao, {foreignKey : 'fornecedor_id'})
Produto.hasMany(Contratacao, { foreignKey: 'produto_id'})
Pacote.hasMany(Promocao, { foreignKey : 'pacote_id'})
Cliente.hasMany(RobotContrato, {foreignKey : 'cliente_id'})
RobotProduto.hasMany(RobotContrato, {foreignKey : 'robotproduto_id'})

sequelize.sync()

module.exports = {
    Associacao,
    SegmentoMercado,
    Cliente,
    ClientePf,
    ClientePj,
    ClienteEndereco,
    ClienteImagem,
    ClienteContato,
    Funcao,
    RelacionamentoCliente,
    Conta,
    Orcamento,
    ItemOrcamento,
    ParametroOrcamento,
    Produto,
    CategoriaProduto,
    ProdutoImagem,
    Fabricante,
    Contratacao,
    Pacote,
    Promocao,
    RobotContrato,
    RobotProduto
}
