require('dotenv').config()

module.exports = { 
    account: process.env.cloudant_username, 
    password: process.env.cloudant_password 
}