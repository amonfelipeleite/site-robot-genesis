require('dotenv').config()

module.exports = {
    username: process.env.mysql_username,
    password: '', //process.env.mysql_password,
    database: 'robtest', //process.env.mysql_database,
    host: process.env.mysql_host,
    dialect: 'mysql',
    timestamps: false,
    logging: function (str) {
        // Não gera log
    }
}