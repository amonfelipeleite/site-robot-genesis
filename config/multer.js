const multer = require('multer')
const path = require('path')
const crypto = require('crypto')
const helpers = require('../resources/js/front-helpers')

const uploadPath = path.resolve(__dirname, '..', 'uploads')

const storageTypes = {
    local: multer.diskStorage({
        destination: (req, file, callback) => {
            callback(null, uploadPath)
        },
        filename: (req, file, callback) => {
            crypto.randomBytes(16, (err, hash) => {
                if(err) callback(err)

                file.key = `${helpers.maskRemove(req.body.cnpj)}-${hash.toString('hex')}${path.extname(file.originalname)}`

                file.url = process.env.environment === 'local' ? 
                    `http://localhost:${process.env.port}/uploads/${file.key}` : 
                    `https://www.robot.rio.br/uploads/${file.key}`

                callback(null, file.key)
            })
        }
    }),
    // s3: multerS3({
    //         s3: new aws.S3(),
    //         bucket: 'robot-objects',
    //         contenType: multerS3.AUTO_CONTENT_TYPE,
    //         acl: 'public-read',
    //         key: (req, file, cb) => {
    //             crypto.randomBytes(16, (err, hash) => {
    //                 if(err) callback(err)
    
    //                 file.key = `${hash.toString('hex')}-${file.originalname}`
    
    //                 callback(null, file.key)
    //             })
    //         }
    //     })
}

module.exports = {
    dest: uploadPath,
    storage: storageTypes['local'],
    limits: {
        fileSize: 2 * 1024 * 1024
    },
    fileFilter: (req, file, callback) => {
        const allowedMimes = [
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/gif'
        ]

        if(!allowedMimes.includes(file.mimetype)) {
            callback(new Error('Tipo de arquivo inválido.'))
        }
        else {
            callback(null, true)
        }
    }
}