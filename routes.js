const express = require('express')
const routes = new express.Router()

routes.get('/', (req, res) => {
    let content = Object.assign({}, pageContent)
    content.navbar = navbar
    res.render('home', content)
})

routes.get('/products', (req, res) => {
    res.sendFile(__dirname + '/in_construct.html')
})

routes.get('/login', (req, res) => {
    if(autenticated) {
        res.redirect('/cadastro')
        return
    }
    
    let content = Object.assign({}, pageContent)
    content.navbar = emptyNavbar

    if(notification) {
        content.msg = notification
        notification = null
    }
    else {
        notification = null
        content.msg = null
    }
    
    res.render('login', content)
})

routes.get('/logoff', (req, res) => {
    autenticated = false
    console.log('MATAR A SESSÃO DO USUÁRIO ============')
    res.redirect('/login')
})

routes.get('/passRecover', (req, res) => {
    notification = helpers.makeToast('Em construção...', 'warning')
    res.redirect('/login')
})

routes.get('/cadastro', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    content.msg = notification

    res.render('cadastro', content)
})

routes.get('/consulta', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    res.render('consulta', content)
})

routes.get('/obterSegmentos', (req, res) => {
    res.sendFile(__dirname + '/data/segmentos.json')
})

routes.get('/obterAssociacoes', (req, res) => {
    res.sendFile(__dirname + '/data/associacoes.json')
})

routes.get('/obterTiposContato', (req, res) => {
    res.sendFile(__dirname + '/data/tipos_contato.json')
})

routes.get('/obterClientes', (req, res) => {
    const clientDB = cloudant.use(process.env.cloudant_client_database)

    clientDB.list({ include_docs: true }, (err, data) => {
        if(err) console.log(err)
        
        try {
            res.json(data.rows)
        }
        catch(err) {
            res.send('O sistema está sem conexão com a base de dados.')
        }
    })
})

routes.post('/cadastro', async (req, res) => {
    if(!autenticated) {
        res.redirect('/login')
        return
    }

    try {
        //Envio dos arquivos
        await fileUpload(req, res)
            .then(() => {})
            .catch(msg => notification = helpers.makeToast(msg, 'error'))

        if(notification) {
            res.redirect('/cadastro')
            return
        }

        //Registro dos dados do cliente
        req.body.fotos = req.files.map(f => f.url)
        registerClientInfo(req.body)

        notification = helpers.makeToast('Cadastrado com sucesso!', 'success')
        res.redirect('/cadastro')
    }
    catch(err) {
        notification = helpers.makeToast(err.message, 'error')
        res.redirect('/cadastro')
    }
})

routes.post('/autenticar', (req, res) => {
    let login = req.body.login
    let senha = req.body.senha

    if(autenticate(login, senha)) {
        res.redirect('/cadastro')
    }
    else {
        res.redirect('/login')
    }
})

module.exports = routes