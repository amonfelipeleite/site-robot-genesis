require('dotenv').config()
const express = require('express')
const app = express()
const fs = require('fs')
const cors = require('cors')
const helpers = require('./resources/js/front-helpers')
const bodyParser = require('body-parser')
const multer = require('multer')
let multerConfig = require('./config/multer')
multerConfig = multer(multerConfig).array('fotos', 5)
const Cloudant = require('@cloudant/cloudant')
const cloudantConfig = require('./config/cloudant.js')
const port = process.env.port
let cloudant
let pageContent
let navbar, navbarCadastro, emptyNavbar
const models = require('./models/models')

//Guardar na sessão do usuário
let autenticated = false
let notification = null

//Middlewares
app.set('view engine', 'ejs')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(express.static('./resources'))
app.use('/uploads', express.static(__dirname + '/uploads'))
//app.use(require('./routes'))

app.listen(port, () => {
    //Load page's static content
    navbar = JSON.parse(fs.readFileSync('data/navbar.json', 'utf8'))
    navbarCadastro = JSON.parse(fs.readFileSync('data/navbar_cadastro.json', 'utf8'))
    emptyNavbar = JSON.parse(fs.readFileSync('data/empty_navbar.json', 'utf8'))
    const footer = JSON.parse(fs.readFileSync('data/footer.json', 'utf8'))
    const members = JSON.parse(fs.readFileSync('data/members.json', 'utf8'))
    const about = JSON.parse(fs.readFileSync('data/about.json', 'utf8'))
    const icons = JSON.parse(fs.readFileSync('data/icons.json', 'utf8'))

    pageContent = {
        domain: process.env.environment == 'local' ? `http://localhost:${port}` : 'https://www.robot.rio.br',
        logo: '/img/logo-white.png',
        title: 'Robot Genesis', 
        subtitle: 'Inovando o que há de novo em tecnologia.',
        navbar,
        footer,
        members,
        about,
        icons
    }

    //Load Cloudant's connection
    cloudant = Cloudant(cloudantConfig)

    console.log(`Site online - Listening on port ${port}`)
})

//Rota para home do site
app.get('/', (req, res) => {
    let content = Object.assign({}, pageContent)
    content.navbar = navbar
    res.render('home', content)
})

//Rota para página de produtos
app.get('/products', (req, res) => {
    res.sendFile(__dirname + '/in_construct.html')
})

//Rota para tela de login
app.get('/login', (req, res) => {
    if(autenticated) {
        res.redirect('/cadastro')
        return
    }
    
    let content = Object.assign({}, pageContent)
    content.navbar = emptyNavbar

    if(notification) {
        content.msg = notification
        notification = null
    }
    else {
        notification = null
        content.msg = null
    }
    
    res.render('login', content)
})

//Rota para logoff
app.get('/logoff', (req, res) => {
    autenticated = false
    console.log('MATAR A SESSÃO DO USUÁRIO ============')
    res.redirect('/login')
})

//Rota para recuperação de senha
app.get('/passRecover', (req, res) => {
    notification = helpers.makeToast('Em construção...', 'warning')
    res.redirect('/login')
})

//Rotas para obtenção das UFs em JSON
app.get('/obter/ufs', (req, res) => {
    res.sendFile(__dirname + '/data/ufs.json')
})

// ======================================== CLIENTE

//Obter dados de cliente
app.get('/obter/Cliente', async (req, res) => {
    // CLOUDANT ===========
    // const clientDB = cloudant.use(process.env.cloudant_client_database)

    // clientDB.list({ include_docs: true }, (err, data) => {
    //     if(err) console.log(err)
        
    //     try {
    //         res.json(data.rows)
    //     }
    //     catch(err) {
    //         res.send('O sistema está sem conexão com a base de dados.')
    //     }
    // })

    let error = false
    let clients = []

    await models.Cliente.findAll({
        include: [
            models.ClientePj, 
            models.Associacao, 
            models.SegmentoMercado, 
            models.ClienteContato, 
            models.ClienteEndereco, 
            models.ClienteImagem 
        ]
    })
        .then(clientes => clients = clientes)
        .catch(err => error = err)

    if(error) throw new Error(error)

    res.json(clients)
})

//Rota para listagem de clientes
app.get('/consulta/Cliente', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    res.render('consulta_cliente', content)
})

//Rota para tela de cadastro de clientes
app.get('/cadastro/Cliente', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    content.msg = notification

    res.render('cadastro_cliente', content)
})

//Cadastro de cliente
app.post('/cadastro/Cliente', async (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    try {
        //Envio dos arquivos
        await fileUpload(req, res)
            .then(() => {})
            .catch(msg => notification = helpers.makeToast(msg, 'error'))

        if(notification) {
            res.redirect('/cadastro/Cliente')
            return
        }

        //Registro dos dados do cliente
        req.body.fotos = req.files.map(f => f.url)
        //registerClientInfo_Cloudant(req.body)

        registerClientInfo_MySQL(req.body)

        notification = helpers.makeToast('Cadastrado com sucesso!', 'success')
        res.redirect('/cadastro/Cliente')
    }
    catch(err) {
        notification = helpers.makeToast(err.message, 'error')
        res.redirect('/cadastro/Cliente')
    }
})

// ======================================== GENÉRICO

//Obter dados de entidade genérica
app.get('/obter/:model', async (req, res) => {
    await models[req.params.model].findAll()
        .then(entries => res.json(entries))
        .catch(err => res.send(err))
})

//Rota para listagem genérica de objetos de uma entidades
app.get('/consulta/:model', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    content.entity = helpers.parseEntityToText(req.params.model)
    content.model = req.params.model
    res.render('consulta', content)
})

//Rota para tela de cadastro genérico
app.get('/cadastro/:model', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    let content = Object.assign({}, pageContent)
    content.navbar = navbarCadastro
    content.msg = notification
    content.entity = helpers.parseEntityToText(req.params.model)
    content.action = req.params.model

    res.render('cadastro', content)
})

//Cadastro genérico
app.post('/cadastro/:model', (req, res) => {
    if(!autenticated) {
        notification = helpers.makeToast('Você deve fazer login.', 'warning') 
        res.redirect('/login')
        return
    }

    try {
        register_MySQL(req.body, models[req.params.model])

        notification = helpers.makeToast('Cadastrado com sucesso!', 'success')
        res.redirect(`/cadastro/${req.params.model}`)
    }
    catch(err) {
        notification = helpers.makeToast(err.message, 'error')
        res.redirect(`/cadastro/${req.params.model}`)
    }
})

// =================================================

//Método de autenticação
app.post('/autenticar', (req, res) => {
    let login = req.body.login
    let senha = req.body.senha

    if(autenticate(login, senha)) {
        res.redirect('/cadastro/Cliente')
    }
    else {
        res.redirect('/login')
    }
})

//Upload de imagens
function fileUpload(req, res) {
    return new Promise((resolve, reject) => {
        multerConfig(req, res, err => {
            if(err) {
                err.message = helpers.translateMessage(err.message)
                reject(helpers.makeToast(err.message, 'error'))
            }

            resolve()
        })
    })
}

//Cadastro do cliente no Cloudant
function registerClientInfo_Cloudant(object) {
    let error = false

    helpers.maskRemove(object, ['site', 'email'])
    helpers.removeWhiteSpaces(object.fone)

    const clientDB = cloudant.use(process.env.cloudant_client_database)

    clientDB.insert(object, object.cnpj.replace(/[\.-\/]/g, ''))
            .then(data => error = !data.ok ? 'Erro ao registrar dados do cliente no Cloudant -> robot_client.' : false)
            .catch(err => error = err)

    if(error) throw new Error(error)
}

// Inserção de Cliente no MySQL (DEVE SER UMA TRANSACTION)
async function registerClientInfo_MySQL(object) {
    helpers.maskRemove(object, ['site', 'email'])
    helpers.removeWhiteSpaces(object.fone)

    let error = false

    //Cadastro do cliente
    let clienteId
    await models.Cliente.create({ 
        pessoa: object.tipo,
        facebook: '',
        site: object.site,
        associacao_id: object.associacao,
        segmento_id: object.segmento 
    })
    .then(cliente => clienteId = cliente.id)
    .catch(err => error = err)

    if(error) throw new Error(error)

    //Cadastro do tipo de pessoa
    switch(object.tipo) {
        case 'PJ':
            await models.ClientePj.create({ 
                cnpj: object.cnpj,
                nomeFantasia: object.fantasia,
                razaoSocial: object.razao,
                cliente_id: clienteId
            })
            .then(clientePJ => console.log('Cliente PJ inserido com o ID = ' + clientePJ.id))
            .catch(err => error = err)
            break
        case 'PF':
            await models.ClientePf.create({ 
                cpf: object.cpf,
                nome: object.nome,
                cliente_id: clienteId
            })
            .then(clientePF => console.log('Cliente PF inserido com o ID = ' + clientePF.id))
            .catch(err => error = err)
            break
    }

    if(error) throw new Error(error)

    //Cadastro dos contatos obtidos da API de CNPJ
    const contactFields = ['fone', 'email']
    for(let i = 0; i< contactFields.length; i++) {
        await models.ClienteContato.create({ 
            tipo: contactFields[i],
            valor: object[contactFields[i]],
            cliente_id: clienteId
        })
        .then(contato => console.log('ClienteContato inserido com o ID = ' + contato.id))
        .catch(err => error = err)
    }

    if(error) throw new Error(error)
    
    //Cadastro do endereço
    await models.ClienteEndereco.create({ 
        cep: object.cep,
        uf: object.uf,
        bairro: object.bairro,
        logradouro: object.logradouro,
        numero: object.numero,
        complemento: object.complemento,
        cliente_id: clienteId
    })
    .then(endereco => console.log('ClienteEndereco inserido com o ID = ' + endereco.id))
    .catch(err => error = err)

    if(error) throw new Error(error)

    //Cadastro das imagens do cliente
    object.fotos.forEach(async fotoUrl => {
        await models.ClienteImagem.create({ 
            urlImagem: fotoUrl,
            cliente_id: clienteId
        })
        .then(clientePJ => console.log('ClienteImagem inserido com o ID = ' + clientePJ.id))
        .catch(err => error = err)
    
        if(error) throw new Error(error)
    })

    return true
}

//Inserção genérica no MySQL
async function register_MySQL(object, modelClass) {
    let error = false
    let entity = {}
    
    Object.keys(object).forEach(key => {
        entity[key] = object[key]
    })

    modelClass.create(entity)
        .then(() => {})
        .catch(err => error = err)

    if(error) throw new Error(error)

    return true
}

//Regra de autenticação
function autenticate(login, senha) {
    let status = true
    let msg = null

    if(login !== 'felipe') {
        status = false
        msg = helpers.makeToast('Seu login está incorreto.', 'error')
    }

    if(senha !== '123') {
        status = false
        msg = helpers.makeToast('Sua senha está incorreta.', 'error')
    }
    
    autenticated = status
    notification = msg

    return autenticated
}
